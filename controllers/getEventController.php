<?php
session_start();

// Get the contents of the JSON file 
// $json = file_get_contents( __DIR__.'\..\models\events.json');
$json = file_get_contents(  __DIR__.'/models/events.json');
$array_data = json_decode($json, true);

//data- the user events
$data = @$array_data[$_SESSION['name']]; 

if ($data == null) {
    exit();
} else {
    echo json_encode($data);
    return;
}