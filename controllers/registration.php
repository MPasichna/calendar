<?php
session_start();
// open and decode db data
$json = file_get_contents(__DIR__ . '/models/passwords.json');
$array_data = json_decode($json, true);

// Takes raw data from the request 
$data = file_get_contents("php://input");

$password = parser($data, "[password:");
$passwordAgain = parser($data, "[passwordAgain:");
$name = parser($data, "[name:");
$email = parser($data, "[email:");

$_SESSION["name"] = $name;


if ($password == $passwordAgain) {
    // if name already exist
    if ($array_data[$name]) {
        echo "exist";
        return;
        exit;
    } else {
        //else write to db new user
        $array_data[$name] =  array(
            'password'     =>   password_hash($password, PASSWORD_BCRYPT),
            'e-mail'       =>   $email
        );

        //zapis dat do db
        $final_data = json_encode($array_data);
        file_put_contents(__DIR__ . '/models/passwords.json', $final_data);

    }
}

//to take data that we need from all data
function parser($data, $key)
{
    $start1 = strpos($data, $key) + strlen($key);
    $end1 = strlen($data) - $start1;
    $shortened = substr($data, $start1, $end1);
    $end2 = strpos($shortened, "]");
    $word = substr($shortened, 0, $end2);
    return $word;
}
