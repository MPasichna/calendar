<?php
session_start();
// Takes raw data from the request 
$data = file_get_contents("php://input");
$password = parser($data, "[password:");
$name = parser($data, "[name:");

// open and decode db data
$json=file_get_contents(__DIR__. '/models/passwords.json');
$array_data = json_decode($json, true);

//control input data
if ((isset($array_data[$name]))) {
        if (password_verify($password, $array_data[$name]['password'])) {
        $_SESSION["name"] = $name;
        // header("Location: ../view/calendar.html");
        echo("corect");
        return;
        }
} else {
        echo "<script type='text/javascript'>
        alert('information is not correct!');
        </script>";
        exit;
}

//to take data that we need from all data
function parser($data, $key)
{
        $start1 = strpos($data, $key) + strlen($key);
        $end1 = strlen($data) - $start1;
        $shortened = substr($data, $start1, $end1);
        $end2 = strpos($shortened, "]");
        $word = substr($shortened, 0, $end2);
        return $word;
}
