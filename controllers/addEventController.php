<?php
session_start();

$name = $_SESSION["name"];
$json = file_get_contents( __DIR__.'/models/events.json');
$array_data = json_decode($json, true);
if (!isset($array_data[$name])) {
    $array_data[$name] = [];
}

array_push($array_data[$name], array(
    'Date'         =>    htmlspecialchars($_POST["Date"]),
    'Title'     =>     htmlspecialchars($_POST["Title"])
));

//write data to db
$final_data = json_encode($array_data);
file_put_contents(  __DIR__.'/models/events.json', $final_data);

