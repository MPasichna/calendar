<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.8.17">
  <compounddef id="md_libraries_tui_8calendar-master__r_e_a_d_m_e" kind="page">
    <compoundname>md_libraries_tui.calendar-master_README</compoundname>
    <title>&lt;img src=&quot;https://user-images.githubusercontent.com/26706716/39230183-7f8ff186-48a0-11e8-8d9c-9699d2d0e471.png&quot; alt=&quot;TOAST UI Calendar&quot;/&gt;</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><blockquote><para>A JavaScript schedule calendar that is full featured. Now your service just got the customizable calendar. </para>
</blockquote></para>
<para><ulink url="https://github.com/nhn/tui.calendar/releases/latest"><image type="html" name="tui.calendar.svg" inline="yes"></image>
</ulink> <ulink url="https://www.npmjs.com/package/tui-calendar"><image type="html" name="tui-calendar.svg" inline="yes"></image>
</ulink> <ulink url="https://github.com/nhn/tui.calendar/blob/master/LICENSE"><image type="html" name="tui.calendar.svg" inline="yes"></image>
</ulink> <ulink url="https://github.com/nhn/tui.project-name/labels/help%20wanted"><image type="html" name="PRs-welcome-ff69b4.svg" inline="yes"></image>
</ulink> <ulink url="https://github.com/nhn"><image type="html" name="%3C%2F%3E%20with%20%E2%99%A5%20by-NHN-ff1414.svg" inline="yes"></image>
</ulink></para>
<para><image type="html" name="55609612-0c19db00-57bc-11e9-849b-f42a9bd8c591.gif" inline="yes"></image>
</para>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md73">
<title>🚩 Table of Contents</title>
<para><itemizedlist>
<listitem><para><ulink url="#Collect-statistics-on-the-use-of-open-source">Collect statistics on the use of open source</ulink></para>
</listitem><listitem><para><ulink url="#-documents">Documents</ulink></para>
</listitem><listitem><para><ulink url="#how-cool-monthly-weekly-daily-and-various-view-types">How Cool: Monthly, Weekly, Daily and Various View Types.</ulink></para>
</listitem><listitem><para><ulink url="#easy-to-use-dragging-and-resizing-a-schedule">Easy to Use: Dragging and Resizing a Schedule</ulink></para>
</listitem><listitem><para><ulink url="#ready-to-use-default-popups">Ready to Use: Default Popups</ulink></para>
</listitem><listitem><para><ulink url="#-features">Features</ulink></para>
</listitem><listitem><para><ulink url="#-examples">Examples</ulink></para>
</listitem><listitem><para><ulink url="#-install">Install</ulink></para>
</listitem><listitem><para><ulink url="#-wrappers">Wrappers</ulink></para>
</listitem><listitem><para><ulink url="#-usage">Usage</ulink></para>
</listitem><listitem><para><ulink url="#-browser-support">Browser Support</ulink></para>
</listitem><listitem><para><ulink url="#-pull-request-steps">Pull Request Steps</ulink></para>
</listitem><listitem><para><ulink url="#-contributing">Contributing</ulink></para>
</listitem><listitem><para><ulink url="#-dependency">Dependency</ulink></para>
</listitem><listitem><para><ulink url="#-toast-ui-family">TOAST UI Family</ulink></para>
</listitem><listitem><para><ulink url="#-used-by">Used By</ulink></para>
</listitem><listitem><para><ulink url="#-license">License</ulink></para>
</listitem></itemizedlist>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md74">
<title>Collect statistics on the use of open source</title>
<para>TOAST UI Calendar applies Google Analytics (GA) to collect statistics on the use of open source, in order to identify how widely TOAST UI Calendar is used throughout the world. It also serves as important index to determine the future course of projects. location.hostname (e.g. &gt; “ui.toast.com") is to be collected and the sole purpose is nothing but to measure statistics on the usage.</para>
<para>To disable GA use the <ulink url="https://nhn.github.io/tui.calendar/latest/global.html#Options">options</ulink>:</para>
<para><programlisting filename=".js"><codeline><highlight class="normal">var<sp/>calendar<sp/>=<sp/>new<sp/>Calendar(&apos;#calendar&apos;,<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>usageStatistics:<sp/>false</highlight></codeline>
<codeline><highlight class="normal">});</highlight></codeline>
</programlisting></para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md75">
<title>📙 Documents</title>
<para><itemizedlist>
<listitem><para><ulink url="https://github.com/nhn/tui.calendar/blob/master/docs/getting-started.md">Getting Started</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.calendar/tree/master/docs">Tutorials</ulink></para>
</listitem><listitem><para><ulink url="https://nhn.github.io/tui.calendar/latest/Calendar">APIs</ulink></para>
</listitem></itemizedlist>
</para>
<para>You can also see the older versions of API page on the <ulink url="https://github.com/nhn/tui.calendar/releases">releases page</ulink>.</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md76">
<title>✨ How Cool: Monthly, Weekly, Daily and Various View Types.</title>
<para><table rows="2" cols="2"><row>
<entry thead="yes"><para>Monthly </para>
</entry><entry thead="yes"><para>Weekly  </para>
</entry></row>
<row>
<entry thead="no"><para><image type="html" name="39230396-4d79a592-48a1-11e8-9849-08e80f1bedf6.png" inline="yes"></image>
 </para>
</entry><entry thead="no"><para><image type="html" name="39230459-83beac38-48a1-11e8-8cd4-11b97817f1f8.png" inline="yes"></image>
  </para>
</entry></row>
</table>
</para>
<para><table rows="2" cols="2"><row>
<entry thead="yes"><para>Daily </para>
</entry><entry thead="yes"><para>2 Weeks  </para>
</entry></row>
<row>
<entry thead="no"><para><image type="html" name="39230685-60a2a1d6-48a2-11e8-9d46-ce5693277a64.png" inline="yes"></image>
 </para>
</entry><entry thead="no"><para><image type="html" name="39230638-281d5266-48a2-11e8-84d8-ab289f372051.png" inline="yes"></image>
  </para>
</entry></row>
</table>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md77">
<title>Easy to Use: Dragging and Resizing a Schedule</title>
<para><table rows="2" cols="2"><row>
<entry thead="yes"><para>Dragging </para>
</entry><entry thead="yes"><para>Resizing  </para>
</entry></row>
<row>
<entry thead="no"><para><image type="html" name="39230930-591031f8-48a3-11e8-8f62-e12e6c19920c.gif" inline="yes"></image>
 </para>
</entry><entry thead="no"><para><image type="html" name="39231671-c926d0da-48a5-11e8-959d-35fd32f2c522.gif" inline="yes"></image>
  </para>
</entry></row>
</table>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md78">
<title>Ready to Use: Default Popups</title>
<para><table rows="2" cols="2"><row>
<entry thead="yes"><para>Creation Popup </para>
</entry><entry thead="yes"><para>Detail Popup  </para>
</entry></row>
<row>
<entry thead="no"><para><image type="html" name="39230798-d151a9ae-48a2-11e8-842d-b19b40432f48.png" inline="yes"></image>
 </para>
</entry><entry thead="no"><para><image type="html" name="39230820-e73fa11c-48a2-11e8-9348-8e3d81979a78.png" inline="yes"></image>
  </para>
</entry></row>
</table>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md79">
<title>🎨 Features</title>
<para><itemizedlist>
<listitem><para>Supports various view types: daily, weekly, monthly(6 weeks, 2 weeks, 3 weeks)</para>
</listitem><listitem><para>Supports efficient management of milestone and task schedules</para>
</listitem><listitem><para>Supports the narrow width of weekend</para>
</listitem><listitem><para>Supports changing start day of week</para>
</listitem><listitem><para>Supports customizing the date and schedule information UI(including a header and a footer of grid cell)</para>
</listitem><listitem><para>Supports adjusting a schedule by mouse dragging</para>
</listitem><listitem><para>Supports customizing UI by theme</para>
</listitem></itemizedlist>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md80">
<title>🐾 Examples</title>
<para><itemizedlist>
<listitem><para><ulink url="https://nhn.github.io/tui.calendar/latest/tutorial-example00-basic">Basic</ulink> : Example of using default options.</para>
</listitem></itemizedlist>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md81">
<title>💾 Install</title>
<para>TOAST UI products can be used by using the package manager or downloading the source directly. However, we highly recommend using the package manager.</para>
<sect2 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md82">
<title>Via Package Manager</title>
<para>TOAST UI products are registered in two package managers, <ulink url="https://www.npmjs.com/">npm</ulink> and <ulink url="https://bower.io/">bower</ulink>. You can conveniently install it using the commands provided by each package manager. When using npm, be sure to use it in the environment <ulink url="https://nodejs.org">Node.js</ulink> is installed.</para>
<sect3 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md83">
<title>npm</title>
<para><programlisting><codeline><highlight class="normal">$<sp/>npm<sp/>install<sp/>--save<sp/>tui-calendar<sp/>#<sp/>Latest<sp/>version</highlight></codeline>
<codeline><highlight class="normal">$<sp/>npm<sp/>install<sp/>--save<sp/>tui-calendar@&lt;version&gt;<sp/>#<sp/>Specific<sp/>version</highlight></codeline>
</programlisting></para>
</sect3>
<sect3 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md84">
<title>bower</title>
<para><programlisting><codeline><highlight class="normal">$<sp/>bower<sp/>install<sp/>tui-calendar<sp/>#<sp/>Latest<sp/>version</highlight></codeline>
<codeline><highlight class="normal">$<sp/>bower<sp/>install<sp/>tui-calendar#&lt;tag&gt;<sp/>#<sp/>Specific<sp/>version</highlight></codeline>
</programlisting></para>
</sect3>
</sect2>
<sect2 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md85">
<title>Via Contents Delivery Network (CDN)</title>
<para>TOAST UI products are available over the CDN powered by <ulink url="https://www.toast.com">TOAST Cloud</ulink>.</para>
<para>You can use the CDN as below.</para>
<para>Insert style sheet files <programlisting filename=".html"><codeline><highlight class="normal">&lt;link<sp/>rel=&quot;stylesheet&quot;<sp/>type=&quot;text/css&quot;<sp/>href=&quot;https://uicdn.toast.com/tui-calendar/latest/tui-calendar.css&quot;<sp/>/&gt;</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">&lt;!--<sp/>If<sp/>you<sp/>use<sp/>the<sp/>default<sp/>popups,<sp/>use<sp/>this.<sp/>--&gt;</highlight></codeline>
<codeline><highlight class="normal">&lt;link<sp/>rel=&quot;stylesheet&quot;<sp/>type=&quot;text/css&quot;<sp/>href=&quot;https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.css&quot;<sp/>/&gt;</highlight></codeline>
<codeline><highlight class="normal">&lt;link<sp/>rel=&quot;stylesheet&quot;<sp/>type=&quot;text/css&quot;<sp/>href=&quot;https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.css&quot;<sp/>/&gt;</highlight></codeline>
</programlisting></para>
<para>Insert JavaScript file <programlisting filename=".html"><codeline><highlight class="normal">&lt;script<sp/>src=&quot;https://uicdn.toast.com/tui.code-snippet/latest/tui-code-snippet.js&quot;&gt;&lt;/script&gt;</highlight></codeline>
<codeline><highlight class="normal">&lt;script<sp/>src=&quot;https://uicdn.toast.com/tui.dom/v3.0.0/tui-dom.js&quot;&gt;&lt;/script&gt;</highlight></codeline>
<codeline><highlight class="normal">&lt;script<sp/>src=&quot;https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.min.js&quot;&gt;&lt;/script&gt;</highlight></codeline>
<codeline><highlight class="normal">&lt;script<sp/>src=&quot;https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.min.js&quot;&gt;&lt;/script&gt;</highlight></codeline>
<codeline><highlight class="normal">&lt;script<sp/>src=&quot;https://uicdn.toast.com/tui-calendar/latest/tui-calendar.js&quot;&gt;&lt;/script&gt;</highlight></codeline>
</programlisting></para>
<para>If you want to use a specific version, use the tag name instead of <computeroutput>latest</computeroutput> in the url&apos;s path.</para>
<para>The CDN directory has the following structure.</para>
<para><programlisting><codeline><highlight class="normal">tui-calendar/</highlight></codeline>
<codeline><highlight class="normal">├─<sp/>latest/</highlight></codeline>
<codeline><highlight class="normal">│<sp/><sp/>├─<sp/>tui-calendar.js</highlight></codeline>
<codeline><highlight class="normal">│<sp/><sp/>└─<sp/>tui-calendar.min.js</highlight></codeline>
<codeline><highlight class="normal">│<sp/><sp/>└─<sp/>tui-calendar.css</highlight></codeline>
<codeline><highlight class="normal">│<sp/><sp/>└─<sp/>tui-calendar.min.css</highlight></codeline>
<codeline><highlight class="normal">├─<sp/>v1.0.0/</highlight></codeline>
<codeline><highlight class="normal">│<sp/><sp/>├─<sp/>...</highlight></codeline>
</programlisting></para>
</sect2>
<sect2 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md86">
<title>Download Source Files</title>
<para><itemizedlist>
<listitem><para><ulink url="https://github.com/nhn/tui.calendar/tree/master/dist">Download bundle files</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.calendar/releases">Download all sources for each version</ulink></para>
</listitem></itemizedlist>
</para>
</sect2>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md87">
<title>🛍 Wrappers</title>
<para><itemizedlist>
<listitem><para><ulink url="https://github.com/nhn/toast-ui.vue-calendar">toast-ui.vue-calendar</ulink> <bold>Vue</bold> wrapper component is implemented by <ulink url="https://github.com/nhn">NHN</ulink>.</para>
</listitem><listitem><para><ulink url="https://github.com/nhn/toast-ui.react-calendar">toast-ui.react-calendar</ulink> <bold>React</bold> wrapper component is powered by <ulink url="https://github.com/nhn">NHN</ulink>.</para>
</listitem><listitem><para><ulink url="https://github.com/brnrds/ngx-tui-dev">ngx-tui-dev</ulink>: <bold>TypeScript</bold> and <bold>Angular 5</bold> wrapper component is being implemented(ref <ulink url="https://github.com/nhn/tui.calendar/issues/82">#82</ulink>) by <ulink url="https://github.com/amanvishnani">@amanvishnani</ulink> and <ulink url="https://github.com/brnrds">@brnrds</ulink>. Thanks for their effort.</para>
</listitem><listitem><para><ulink url="https://github.com/lkmadushan/vue-tuicalendar">vue-tui-calendar</ulink>: <bold>Vue</bold> wrapper component is being implemented(ref <ulink url="https://github.com/nhn/tui.calendar/issues/81">#81</ulink>) by <ulink url="https://github.com/lkmadushan">@lkmadushan</ulink>. Thanks for their effort.</para>
</listitem><listitem><para><ulink url="https://github.com/IniZio/react-tui-calendar">tui-calendar-react</ulink>: <bold>React</bold> wrapper component is provided(ref #<ulink url="https://github.com/nhn/tui.calendar/issues/134">134</ulink>) by <ulink url="https://github.com/IniZio">@IniZio</ulink>. Thanks for his effort.</para>
</listitem></itemizedlist>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md88">
<title>🔨 Usage</title>
<sect2 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md89">
<title>HTML</title>
<para>Place a <computeroutput>&lt;div&gt;&lt;/div&gt;</computeroutput> where you want TOAST UI Calendar rendered.</para>
<para><programlisting filename=".html"><codeline><highlight class="normal">&lt;body&gt;</highlight></codeline>
<codeline><highlight class="normal">...</highlight></codeline>
<codeline><highlight class="normal">&lt;div<sp/>id=&quot;calendar&quot;<sp/>style=&quot;height:<sp/>800px;&quot;&gt;&lt;/div&gt;</highlight></codeline>
<codeline><highlight class="normal">...</highlight></codeline>
<codeline><highlight class="normal">&lt;/body&gt;</highlight></codeline>
</programlisting></para>
</sect2>
<sect2 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md90">
<title>JavaScript</title>
<sect3 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md91">
<title>Using namespace in browser environment</title>
<para><programlisting filename=".javascript"><codeline><highlight class="normal">var<sp/>Calendar<sp/>=<sp/>tui.Calendar;</highlight></codeline>
</programlisting></para>
</sect3>
<sect3 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md92">
<title>Using module format in node environment</title>
<para><programlisting filename=".javascript"><codeline><highlight class="normal">var<sp/>Calendar<sp/>=<sp/>require(&apos;tui-calendar&apos;);<sp/>/*<sp/>CommonJS<sp/>*/</highlight></codeline>
<codeline><highlight class="normal">require(&quot;tui-calendar/dist/tui-calendar.css&quot;);</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">//<sp/>If<sp/>you<sp/>use<sp/>the<sp/>default<sp/>popups,<sp/>use<sp/>this.</highlight></codeline>
<codeline><highlight class="normal">require(&quot;tui-date-picker/dist/tui-date-picker.css&quot;);</highlight></codeline>
<codeline><highlight class="normal">require(&quot;tui-time-picker/dist/tui-time-picker.css&quot;);</highlight></codeline>
</programlisting></para>
<para><programlisting filename=".javascript"><codeline><highlight class="normal">import<sp/>Calendar<sp/>from<sp/>&apos;tui-calendar&apos;;<sp/>/*<sp/>ES6<sp/>*/</highlight></codeline>
<codeline><highlight class="normal">import<sp/>&quot;tui-calendar/dist/tui-calendar.css&quot;;</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">//<sp/>If<sp/>you<sp/>use<sp/>the<sp/>default<sp/>popups,<sp/>use<sp/>this.</highlight></codeline>
<codeline><highlight class="normal">import<sp/>&apos;tui-date-picker/dist/tui-date-picker.css&apos;;</highlight></codeline>
<codeline><highlight class="normal">import<sp/>&apos;tui-time-picker/dist/tui-time-picker.css&apos;;</highlight></codeline>
</programlisting></para>
<para>Then you can create a calendar instance with <ulink url="https://nhn.github.io/tui.calendar/latest/Options">options</ulink> to set configuration.</para>
<para><programlisting filename=".javascript"><codeline><highlight class="normal">var<sp/>calendar<sp/>=<sp/>new<sp/>Calendar(&apos;#calendar&apos;,<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>defaultView:<sp/>&apos;month&apos;,</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>taskView:<sp/>true,</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>template:<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>monthDayname:<sp/>function(dayname)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/>return<sp/>&apos;&lt;span<sp/>class=&quot;calendar-week-dayname-name&quot;&gt;&apos;<sp/>+<sp/>dayname.label<sp/>+<sp/>&apos;&lt;/span&gt;&apos;;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>...</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal">});</highlight></codeline>
</programlisting></para>
<para>Or you can use jquery plugin. You must include jquery before using this jquery plugin.</para>
<para><programlisting filename=".js"><codeline><highlight class="normal">$(&apos;#calendar&apos;).tuiCalendar({</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>defaultView:<sp/>&apos;month&apos;,</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>taskView:<sp/>true,</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>template:<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>monthDayname:<sp/>function(dayname)<sp/>{</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/><sp/><sp/>return<sp/>&apos;&lt;span<sp/>class=&quot;calendar-week-dayname-name&quot;&gt;&apos;<sp/>+<sp/>dayname.label<sp/>+<sp/>&apos;&lt;/span&gt;&apos;;</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/><sp/><sp/>...</highlight></codeline>
<codeline><highlight class="normal"><sp/><sp/>}</highlight></codeline>
<codeline><highlight class="normal">});</highlight></codeline>
</programlisting></para>
</sect3>
</sect2>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md93">
<title>🌏 Browser Support</title>
<para><table rows="2" cols="5"><row>
<entry thead="yes"><para><image type="html" name="34348387-a2e64588-ea4d-11e7-8267-a43365103afe.png" inline="yes"></image>
 Chrome </para>
</entry><entry thead="yes"><para><image type="html" name="34348590-250b3ca2-ea4f-11e7-9efb-da953359321f.png" inline="yes"></image>
 Internet Explorer </para>
</entry><entry thead="yes"><para><image type="html" name="34348380-93e77ae8-ea4d-11e7-8696-9a989ddbbbf5.png" inline="yes"></image>
 Edge </para>
</entry><entry thead="yes"><para><image type="html" name="34348394-a981f892-ea4d-11e7-9156-d128d58386b9.png" inline="yes"></image>
 Safari </para>
</entry><entry thead="yes"><para><image type="html" name="34348383-9e7ed492-ea4d-11e7-910c-03b39d52f496.png" inline="yes"></image>
 Firefox  </para>
</entry></row>
<row>
<entry thead="no"><para>Yes </para>
</entry><entry thead="no"><para>+9 </para>
</entry><entry thead="no"><para>Yes </para>
</entry><entry thead="no"><para>Yes </para>
</entry><entry thead="no"><para>Yes  </para>
</entry></row>
</table>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md94">
<title>🔧 Pull Request Steps</title>
<para>TOAST UI products are open source, so you can create a pull request(PR) after you fix issues. Run npm scripts and develop yourself with the following process.</para>
<sect2 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md95">
<title>Setup</title>
<para>Fork <computeroutput>develop</computeroutput> branch into your personal repository. Clone it to local computer. Install node modules. Before starting development, you should check to haveany errors.</para>
<para><programlisting><codeline><highlight class="normal">$<sp/>git<sp/>clone<sp/>https://github.com/{owner}/tui.calendar.git</highlight></codeline>
<codeline><highlight class="normal">$<sp/>cd<sp/>tui.calendar</highlight></codeline>
<codeline><highlight class="normal">$<sp/>npm<sp/>install</highlight></codeline>
<codeline><highlight class="normal">$<sp/>npm<sp/>run<sp/>test</highlight></codeline>
</programlisting></para>
</sect2>
<sect2 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md96">
<title>Develop</title>
<para>Let&apos;s start development! You can see your code is reflected as soon as you saving the codes by running a server. Don&apos;t miss adding test cases and then make green rights.</para>
<sect3 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md97">
<title>Run webpack-dev-server</title>
<para><programlisting><codeline><highlight class="normal">$<sp/>npm<sp/>run<sp/>serve</highlight></codeline>
</programlisting></para>
</sect3>
<sect3 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md98">
<title>Run karma test</title>
<para><programlisting><codeline><highlight class="normal">$<sp/>npm<sp/>run<sp/>test</highlight></codeline>
</programlisting></para>
</sect3>
</sect2>
<sect2 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md99">
<title>Pull Request</title>
<para>Before PR, check to test lastly and then check any errors. If it has no error, commit and then push it!</para>
<para>For more information on PR&apos;s step, please see links of Contributing section.</para>
</sect2>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md100">
<title>💬 Contributing</title>
<para><itemizedlist>
<listitem><para><ulink url="https://github.com/nhn/tui.calendar/blob/master/CODE_OF_CONDUCT.md">Code of Conduct</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.calendar/blob/master/CONTRIBUTING.md">Contributing guideline</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.calendar/blob/master/docs/ISSUE_TEMPLATE.md">Issue guideline</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.calendar/blob/master/docs/COMMIT_MESSAGE_CONVENTION.md">Commit convention</ulink></para>
</listitem></itemizedlist>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md101">
<title>🔩 Dependency</title>
<para><itemizedlist>
<listitem><para><ulink url="https://github.com/nhn/tui.code-snippet">tui-code-snippet</ulink> &gt;= 1.5.0</para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.date-picker">tui-date-picker</ulink> &gt;= 4.0.2 is optional.</para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.time-picker">tui-time-picker</ulink> &gt;= 2.0.1 is optional.</para>
</listitem></itemizedlist>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md102">
<title>🍞 TOAST UI Family</title>
<para><itemizedlist>
<listitem><para><ulink url="https://github.com/nhn/tui.grid">TOAST UI Grid</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.chart">TOAST UI Chart</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.editor">TOAST UI Editor</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn/tui.image-editor">TOAST UI Image-Editor</ulink></para>
</listitem><listitem><para><ulink url="https://github.com/nhn?q=tui">TOAST UI Components</ulink></para>
</listitem></itemizedlist>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md103">
<title>🚀 Used By</title>
<para><itemizedlist>
<listitem><para><ulink url="https://dooray.com">TOAST Dooray! - Collaboration Service (Project, Messenger, Mail, Calendar, Drive, Wiki, Contacts)</ulink></para>
</listitem><listitem><para><ulink url="https://www.e-ncp.com/">NCP - Commerce Platform</ulink></para>
</listitem><listitem><para><ulink url="https://www.godo.co.kr/shopby/main.gd">shopby</ulink></para>
</listitem><listitem><para><ulink url="https://shopping.payco.com/">payco-shopping</ulink></para>
</listitem><listitem><para><ulink url="https://teacher.iamservice.net">iamTeacher</ulink></para>
</listitem><listitem><para><ulink url="https://www.linder.kr">linder</ulink></para>
</listitem></itemizedlist>
</para>
</sect1>
<sect1 id="md_libraries_tui_8calendar-master__r_e_a_d_m_e_1autotoc_md104">
<title>📜 License</title>
<para>This software is licensed under the <ulink url="https://github.com/nhn/tui.calendar/blob/master/LICENSE">MIT</ulink> © <ulink url="https://github.com/nhn">NHN</ulink>. </para>
</sect1>
    </detaileddescription>
  </compounddef>
</doxygen>
