// Select element from HTML DOM.
var formEl = document.querySelector("#form");

// Listen on events
formEl.addEventListener("submit", function (event) {
    event.preventDefault();
    validateForm();
});   

function validateForm() {
    var errors = [];

    var nameEl = document.querySelector("#name");
    var nameValue = nameEl.value;

    if (nameValue.length === 0) {
        errors.push({
            message: "Please fill in your name.",
            element: nameEl
        });
    }

    var emailEl = document.querySelector("#email");
    var emailValue = emailEl.value;

    if (emailValue.length === 0) {
        errors.push({
            message: "Please fill in email.",
            element: emailEl
        });
    } else if (emailValue.length < 5) {
        errors.push({
            message: "Email is too short, minimum is 5 characters.",
            element: emailEl
        });
    } else if (emailValue.length > 30) {
        errors.push({
            message: "Email is too long, maximum is 30 characters.",
            element: emailEl
        });
    } else if (!emailValue.includes("@")) {
        errors.push({
            message: "Email is badly formatted, it does not include the \"@\" character.",
            element: emailEl
        });
    }

    var passwordEl = document.querySelector("#password");
    var passwordAgainEl = document.querySelector("#password-again");
    var passwordValue = passwordEl.value;
    var passwordAgainValue = passwordAgainEl.value;

    if (passwordValue.length === 0) {
        errors.push({
            message: "Please fill in password.",
            element: passwordEl
        });
    } else if (passwordAgainValue.length === 0) {
        errors.push({
            message: "Please fill in password.",
            element: passwordAgainEl
        });
    } else if (passwordValue.length < 5) {
        errors.push({
            message: "Password is too short, minimum is 5 characters.",
            element: passwordEl
        });
    } else if (passwordValue.length > 100) {
        errors.push({
            message: "Password is too long, maximum is 100 characters.",
            element: passwordEl
        });
    } else if (passwordValue !== passwordAgainValue) {
        errors.push({
            message: "Passwords are not the same.",
            element: passwordEl
        });
    }

    var data = "[name:"+nameValue+"][password:" + passwordValue + "][email:" + emailValue + "][passwordAgain:"+ passwordAgainValue + "]" ;

    function AddUser(data) { 
        $.ajax({
            type: 'POST',
            url: '/~pasicmar/controllers/registration.php',
            data: data,
            success: function (result) {
                if(result=='exist'){
                alert("sorry, this username already exist. Pleace, choose another one");
                }
                else{
                    window.location.replace("/~pasicmar/view/calendar.html");
                }
            },
            error: function(error){
              }
        })

    }



    var formValid = errors.length === 0;
    var errorMessageEl = document.querySelector("#error");
    if (formValid) {
        AddUser(data);
        errorMessageEl.hidden = true;
        errorMessageEl.innerText = "";
    } else {
        var firstError = errors[0];
        errorMessageEl.hidden = false;
        errorMessageEl.innerText = firstError.message;
        firstError.element.focus();
    }

    return formValid;
}
