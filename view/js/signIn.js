// Select element from HTML DOM.
var formEl = document.querySelector("#form");

// Listen on events
formEl.addEventListener("submit", function (event) {
    event.preventDefault();
    validateForm();
});

function validateForm() {
    var errors = [];

    var nameEl = document.querySelector("#name");
    var nameValue = nameEl.value;

    if (nameValue.length === 0) {
        errors.push({
            message: "Please fill in your name.",
            element: nameEl
        });
    }

    var passwordEl = document.querySelector("#password");
    var passwordValue = passwordEl.value;

    if (passwordValue.length === 0) {
        errors.push({
            message: "Please fill in password.",
            element: passwordEl
        });
    }

    var data = "[name:" + nameValue + "][password:" + passwordValue + "]";
    function checkPassword(data) {
        $.ajax({
            type: 'POST',
            url: '/~pasicmar/controllers/controllerSignIn.php',
            data: data,
            success: function (result) {
                if (result == "corect") {
                    window.location.replace("/~pasicmar/view/calendar.html");
                }
                else {
                    alert("username or password is not corect")
                }
            },
            error: function () {
                // alert(error);
            }
        })

    }
    var formValid = errors.length === 0;
    var errorMessageEl = document.querySelector("#error");


    if (formValid) {
        checkPassword(data);
        errorMessageEl.hidden = true;
        errorMessageEl.innerText = "";
    } else {
        var firstError = errors[0];
        errorMessageEl.hidden = false;
        errorMessageEl.innerText = firstError.message;
        firstError.element.focus();
    }
    return formValid;

}
